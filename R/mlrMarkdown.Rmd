title: "day 9"
author: "Dawid Wolhuter"
date: "16 May 2019"
output:
  html_document:
  toc:TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
# Initial Steps
##Importing the libraries:
```{r}
library("mlr")
library("knitr")
library("OpenML")
library("ggplot2")
library("ranger")
library("dplyr")
library("e1071")
library("gbm")
library("mlrMBO")
library("glmnet")
library("xgboost")
library("DiceKriging")
library("rgenoud")
library("mmpf")
# packages to produce nice tables and visualizations
library("DataExplorer")
library("rpart.plot")
library("skimr")
library("broom")
library("DT")

#install.packages("nycflights13")
library(nycflights13)
#package for parallellization
library(parallelMap)

# We will use half of the cores available
parallelStartSocket(parallel::detectCores() )
```
##Import data
```{r}
# With base R:
```

##Final dataset
```{r}
#we assign the final data variable to the cleaned data set. for easy generic use.
merge_airlines <- merge(flights, airlines, by = "carrier", all.x = TRUE)
merge_planes <- merge(merge_airlines, planes, by = "tailnum", all.x = TRUE, suffixes = c("_flights", "_planes"))
merge_airports_origin <- merge(merge_planes, airports, by.x = "origin", by.y = "faa", all.x = TRUE, suffixes = c("_carrier", "_origin"))
Final_data <- merge(merge_airports_origin, airports, by.x = "dest", by.y = "faa", all.x = TRUE, suffixes = c("_origin", "_dest"))

Final_data$dep_delay_Y_N <- ifelse(Final_data$dep_delay >0,"Y","N")

Final_Data <- select(Final_data,-c(dep_delay,arr_delay,dest,origin,tailnum,carrier,name_carrier,type,manufacturer,model,engine,name_origin,dst_origin,tzone_origin,dst_dest,tzone_dest,time_hour,name))

#setting the seed for reproducibility
set.seed(123)
```

Introductory Date Exploration
```{r}

plot_intro(Final_Data)
plot_missing(Final_Data)
Final_Data<-na.omit(Final_Data)
plot_intro(Final_Data)
plot_missing(Final_Data)
#Final_Data <- Final_Data[complete.cases(Final_Data), ]

```

```{r}
#we need to split our data into a training and testing dataset. Below is an example of the index

index <- sample(1:nrow(Final_Data) ,nrow(Final_Data)*0.75)
index_train =  index
index_eval  = -index


#dstrain=ds[index,]
 
# index_train  <- index(sample(seq_len(nrow(Final_Data)), size = smp_size))
# index_eval  = index(-index_train)



#Specify the target and data names.

y_target = "dep_delay_Y_N"
data_label = "Depart Delay"

summarizeColumns(Final_Data)
```

#Identify model
```{r}
#Below we define the classification task, along with the target variable and the training dataset,
task = makeClassifTask(id = data_label, target = y_target, data = Final_Data[index_train, ])

#Choosing the main learners to run, these will be tuned, other basic learners will be added later on for comparison.
learner_NB = makeLearner("classif.naiveBayes",predict.type = "prob")
learner_gbm = makeLearner("classif.gbm",predict.type = "prob")
learner_xgboost = makeLearner("classif.xgboost",predict.type = "prob")

task.smote = smote(task, rate = 8, nn = 5)
table(getTaskTargets(task))
table(getTaskTargets(task.smote))


# Here you define the resampling method, CV, LOO, RepCV, Bootstrap, Subsample, Holdout, GrowingWindowCV, FixedWindowCV. And the number if iterations Smaller = faster.
rdesc = makeResampleDesc(method = "Bootstrap", iters = 10, stratify = TRUE)

# Fix instance for a later comparison:
resample_instance = makeResampleInstance(desc = rdesc, task = task.smote)

#We indicate which measures we are interested in. listMeasures() to find the list.
used_measures = list(tpr,fpr,tnr,acc)

```


#Tuning parameters
```{r}
#An important step, these parameters depends on the parameters available for the selected models above. use getParamSet("model_name") to get the list of available hyoer parameters
 
#Ranger set
par_set_NB = makeParamSet(
  makeNumericParam("laplace", lower = 1, upper = 5)
)

#gbm set
par_set_gbm = makeParamSet(
  makeIntegerParam("n.trees", lower = 50, upper = 3000)
  , makeIntegerParam("interaction.depth", lower = 1, upper = 10)
  , makeNumericParam("shrinkage", lower = 0.01, upper = 0.1)
)
getParamSet("classif.naiveBayes")
#Xgboost set
par_set_xgboost = makeParamSet(
  makeIntegerParam("max_depth", lower = 1, upper = 10),
  makeNumericParam("gamma", lower = 0.01, upper = 0.25),
  makeNumericParam("lambda", lower = 0.001, upper = 0.1),
  makeNumericParam("eta", lower = 0.1, upper = 0.5),
  makeNumericParam("min_child_weight",lower=0.5, upper = 1)
  )

```
#Tuning COntroller

```{r}
# The tuning strategy gets defined here. These are called makeTuneControl[Type]().
tune_ctrl = makeTuneControlMBO()

#Combining the gbm learner with the tuning strategy for nested resampling. Define the measure
learner_resample_gbm = makeTuneWrapper(learner = learner_gbm, resampling = cv3, measures = tpr,
  par.set = par_set_gbm, control = tune_ctrl, show.info = TRUE)

#Combining the ranger learner with the tuning strategy for nested resampling. Define the measure
learner_resample_NB = makeTuneWrapper(learner = learner_NB, resampling = cv3, measures = tpr,
  par.set = par_set_NB, control = tune_ctrl, show.info = TRUE)

#Combining the xgboost learner with the tuning strategy for nested resampling. Define the measure
learner_resample_xgboost = makeTuneWrapper(learner = learner_xgboost, resampling = cv3, measures = tpr,
  par.set = par_set_xgboost, control = tune_ctrl, show.info = TRUE)
```

#Benchmark

```{r}
#Making the learnings, and adding additional ones for comparison.
# temp = list(
#   makeLearner("classif.featureless", predict.type = "prob"),
#   makeLearner("classif.rpart", predict.type = "prob"),
#   makeLearner("classif.glmnet", predict.type = "prob"),
#   makeLearner("classif.naiveBayes", predict.type = "prob"),
#   learner_resample_gbm
#   )


learner_list = list(  
  learner_resample_xgboost,
  makeLearner("classif.naiveBayes",predict.type = "prob")
  )

bm_res = benchmark(learners = learner_list, tasks = task.smote,
  measures = used_measures, resamplings = resample_instance)
```

##Results
```{r}
bm_res
```

# getting feature importance
```{r}
model = mlr::train(learner=learner_resample_xgboost,task = task.smote)
feat.imp = getFeatureImportance(model)$res
data = data.frame(Feature = names(feat.imp), Importance = as.numeric(feat.imp))

library(ggplot2)
ggplot(data, aes(x = reorder(Feature, Importance), y = Importance)) +
  geom_bar(stat = "identity") +
  coord_flip() +
  ylab("")
```
#Partial Dependance
```{r}
top_features = feat.imp[order(feat.imp,decreasing = TRUE)][,1:10]
pdp = generatePartialDependenceData(model, input = getTaskData(task), features = names(top_features))
plotPartialDependence(pdp)
```

#PCA
```{r}
pca_df <- na.omit(Final_Data)

# plot_prcomp(pca_df, variance_cap = 0.9, nrow = 2L, ncol = 2L)

```


#Evaluate
```{r}
#Tune parameters now on the complete training set.
tuned_params_xgboost = tuneParams(learner_xgboost, task.smote, cv3, tpr, par_set_xgboost, tune_ctrl)


#set the optimal parameters for the model
learner_xgboost = setHyperPars(learner_xgboost, par.vals = tuned_params_xgboost$x)

#creating a new task on the entire dataset.
full_task = makeClassifTask(id = data_label, target = y_target, data = Final_Data)

#creating a holdout set for testing
holdout = makeFixedHoldoutInstance(train.inds = index_train, test.inds = index_eval, size = nrow(Final_Data))

# recreating the learner list.
learner_list = list(
learner_xgboost
)
evaluation = benchmark(learners = learner_list, tasks = full_task,resamplings = makeResampleDesc("Holdout"),measures = used_measures)
# evaluation = benchmark(learners = learner_list, tasks = full_task,resamplings = holdout, measures = used_measures )


```
##Results
```{r}
evaluation

#Stopping the parrelization
parallelStop()
```

#Visualize
```{r}
library(ggplot2)

task = makeClassifTask(id = data_label, target = y_target, data = Final_Data)

#change to the best learner
learner = learner_xgboost

final_model = train(learner = learner, task = task, subset = index_train)

pred = predict(final_model, newdata = Final_Data[index_eval, ])

plot_data = pred$data
plot_data$Accuracy = cut(x = abs(plot_data$truth - plot_data$response), breaks = c(0, 500, 5000, Inf), labels = c("good", "ok", "critical"))

ggplot() +
  geom_point(data = plot_data, mapping = aes(x = response, y = truth, color = Accuracy), alpha = 0.5) +
  scale_color_brewer(palette = "PuBu") +
  xlab("Predicted") + ylab("Actual")
```

#Create final model
```{r}
task = makeClassifTask(id = data_label, target = y_target, data = Final_Data)
#Select the best learner
learner = learner_xgboost

learner = setHyperPars(learner, par.vals = tuned_params_xgboost$x)

final_model = train(learner = learner, task = task)
```
